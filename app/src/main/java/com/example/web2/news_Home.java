package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class news_Home extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_news__home );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void nnn(View view) {
        Intent job=new Intent(news_Home.this,news_list.class);
        startActivity(job);
    }

    public void onlikclik(View view) {
        Intent job=new Intent(news_Home.this,online_news_list.class);
        startActivity(job);
    }
}
