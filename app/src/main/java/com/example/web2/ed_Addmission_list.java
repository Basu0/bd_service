package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ed_Addmission_list extends AppCompatActivity {
    public ListView listView;
    public String[] edaddmission;
    public int[] edaddmissionimg={R.drawable.nubd,R.drawable.dhakaeducationboard,R.drawable.bute,R.drawable.duet,R.drawable.aiub,R.drawable.iub_logo,R.drawable.nort,R.drawable.bangladesh_medical};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView( R.layout.activity_ed__addmission_list );
        listView=(ListView)findViewById(R.id.addmission_List_id);
        edaddmission=getResources().getStringArray(R.array.ed_addmission);
        addmissionAddptor rw=new addmissionAddptor(this,edaddmissionimg,edaddmission);
        listView.setAdapter( rw );

        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(position==0){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://nubd.info/");
                    startActivity( inty);

                }
                if(position==1){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","https://dhakaeducationboard.gov.bd/index.php/site/subdomain");
                    startActivity( inty);

                }
                if(position==2){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.buet.ac.bd/");
                    startActivity( inty);

                }
                if(position==3){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.duet.ac.bd/admission/undergraduate-admission/");
                    startActivity( inty);

                }
                if(position==4){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.aiub.edu/");
                    startActivity( inty);

                }
                if(position==5){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.iub.edu.bd/");
                    startActivity( inty);

                }
                if(position==6){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","https://nub.ac.bd/");
                    startActivity( inty);

                }
                if(position==7){
                    Intent inty=new Intent(ed_Addmission_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.bmc-bd.org/page/content/notice");
                    startActivity( inty);

                }
            }
        } );
    }
}
