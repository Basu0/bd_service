package com.example.web2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class call_adptor extends BaseAdapter {
    int[] edaddmissionimg ;
     public String[] edaddmission;
    Context context;
    public LayoutInflater inflater;
    call_adptor (Context context,int[] edaddmissionimg,String[] edaddmission){
        this.context=context;
        this.edaddmission=edaddmission;
        this.edaddmissionimg=edaddmissionimg;


    }
    @Override
    public int getCount() {
        return edaddmission.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
        inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView= inflater.inflate(R.layout.call_symop_layout,parent,false);

    }
        ImageView Imageview =(ImageView)convertView.findViewById(R.id.call_image);
        Imageview.setImageResource(edaddmissionimg[position]);
        TextView textView=(TextView)convertView.findViewById(R.id.call_text);
        textView.setText(edaddmission[position]);
        return convertView;

    }}
