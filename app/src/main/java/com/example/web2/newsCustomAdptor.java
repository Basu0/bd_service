package com.example.web2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class newsCustomAdptor extends BaseAdapter {
    String[] newname;
    int[] newsimage;
    Context context;
    public LayoutInflater inflater;

    newsCustomAdptor(Context context,int[] newsimage,String[] newname){
        this.context=context;
        this.newname=newname;
        this.newsimage=newsimage;

    }
    @Override
    public int getCount() {
        return newname.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.news_sampol_layout,parent,false);

        }
        ImageView Imageview =(ImageView)convertView.findViewById(R.id.news_img_id);
        TextView textView=(TextView)convertView.findViewById(R.id.news_tx_id);
        Imageview.setImageResource(newsimage[position]);
        textView.setText(newname[position]);


        return convertView;

    }
}
