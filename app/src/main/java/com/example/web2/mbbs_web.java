package com.example.web2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class mbbs_web extends AppCompatActivity {
    WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.chakri_web );
        webView=(WebView) findViewById(R.id.chakriweb);
        webView.setWebViewClient(new WebViewClient() );
        WebSettings ws=webView.getSettings();
        ws.setJavaScriptEnabled(true);

        webView.loadUrl("http://result.dghs.gov.bd/mbbs/");
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();

        }
        else {
            super.onBackPressed();}
    }
}