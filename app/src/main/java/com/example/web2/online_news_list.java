package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class online_news_list extends AppCompatActivity {
    public ListView listView;
    customadpatorlis cu;
    public String[] onlinnewname;
    public int[] newsimage={R.drawable.download,R.drawable.download1,R.drawable.download2,R.drawable.download3,R.drawable.download4,R.drawable.download5,
            R.drawable.download6,R.drawable.download7,R.drawable.download8,R.drawable.download9,R.drawable.download10,R.drawable.download11,R.drawable.download12,R.drawable.download13
            ,R.drawable.download14,R.drawable.download15,R.drawable.download16,R.drawable.download17,R.drawable.download18,R.drawable.download19};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView( R.layout.activity_online_news_list );
        listView=(ListView)findViewById( R.id.online_news_List_id);
        onlinnewname=getResources().getStringArray(R.array.online_news);
         cu=new customadpatorlis(this, newsimage, onlinnewname);
        listView.setAdapter(cu);
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(position==0){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://bangla.bdnews24.com/");
                    startActivity( inty);

                }

                if(position==1){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.banglanews24.com/");
                    startActivity( inty);

                }
                if(position==2){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.bssnews.net/bangla/");
                    startActivity( inty);

                }
                if(position==3){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.24livenewspaper.com/");
                    startActivity( inty);

                }
                if(position==4){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.jagonews24.com/");
                    startActivity( inty);

                }
                if(position==5){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.amadershomoy.com/bn/");
                    startActivity( inty);

                }
                if(position==6){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.risingbd.com/");
                    startActivity( inty);

                }
                if(position==7){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.rtvonline.com/");
                    startActivity( inty);

                }
                if(position==8){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.priyo.com/");
                    startActivity( inty);

                }
                if(position==9){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dhakatimes24.com/");
                    startActivity( inty);

                }
                if(position==10){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bd24live.com/bangla/");
                    startActivity( inty);

                }
                if(position==11){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.poriborton.com/");
                    startActivity( inty);

                }
                if(position==12){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.ntvbd.com/");
                    startActivity( inty);

                }
                if(position==13){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.sheershanews24.com/");
                    startActivity( inty);

                }
                if(position==14){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://sarabangla.net/");
                    startActivity( inty);

                }
                if(position==15){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.channelionline.com/");
                    startActivity( inty);

                }
                if(position==16){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bbc.com/bengali");
                    startActivity( inty);

                }
                if(position==17){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.dw.com");
                    startActivity( inty);

                }
                if(position==18){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://zeenews.india.com/bengali/");
                    startActivity( inty);

                }
                if(position==19){
                    Intent inty=new Intent(online_news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.anandabazar.com/");
                    startActivity( inty);

                }
            }
        } );
    }
}
