package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class joblistview extends AppCompatActivity {
public ListView joblist;
public String[] jobnamelist;
public int[] jobimage={R.drawable.chakri,R.drawable.bdjobs,R.drawable.jobsinbd,R.drawable.jobsa1,R.drawable.bdcareer,R.drawable.bdjobstoday,
        R.drawable.everyjobs,R.drawable.jobcom,R.drawable.skill,R.drawable.jobstreet};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView( R.layout.activity_jobpage );
        joblist=(ListView)findViewById( R.id.joblist);
        jobnamelist=getResources().getStringArray(R.array.jobList);
        customadpatorlis cu=new customadpatorlis(this, jobimage, jobnamelist);
        joblist.setAdapter(cu);
     joblist.setOnItemClickListener( new AdapterView.OnItemClickListener() {
         @Override
         public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
         if (position==0){
             Intent intf=new Intent(joblistview.this,bd_career_web.class);
             intf.putExtra("url","https://www.chakri.com/");
             startActivity( intf );
          }
          else if (position==1){
             Intent intf=new Intent(joblistview.this,bdjobs_web.class);
             startActivity( intf );

         }
         else if (position==2){
             Intent intf=new Intent(joblistview.this,jobsinabd_web.class);
             startActivity( intf );

         }
         else if (position==3){
             Intent intf=new Intent(joblistview.this,jobsa1.class);
             startActivity( intf );

         }
         else if (position==4){
             Intent intf=new Intent(joblistview.this,bd_career_web.class);
             intf.putExtra("url","http://www.bd-career.com/");
             startActivity( intf );

         }
         else if (position==5){
             Intent intf=new Intent(joblistview.this,bdjobstoday_web.class);
             startActivity( intf );

         }
         else if (position==6){
             Intent intf=new Intent(joblistview.this,everyjobs_web.class);
             startActivity( intf );

         }
         else if (position==7){
             Intent intf=new Intent(joblistview.this,jobcom_web.class);
             startActivity( intf );

         }
         else if (position==8){
             Intent intf=new Intent(joblistview.this,skilljob_web.class);
             startActivity( intf );

         }
         else if (position==9){
             Intent intf=new Intent(joblistview.this,jobstreet_web.class);
             startActivity( intf );

         }
         }
     } );

    }
}
