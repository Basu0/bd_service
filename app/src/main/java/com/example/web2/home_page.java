package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

public class home_page extends AppCompatActivity {
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle toggle;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.home_page );
        drawerLayout=(DrawerLayout)findViewById(R.id.drawerid);
        toggle=new ActionBarDrawerToggle(this,drawerLayout,R.string.Open,R.string.Close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (toggle.onOptionsItemSelected(item)){

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
    public void jobclick(View view) {
        Intent job=new Intent(home_page.this,joblistview.class);
        startActivity(job);
    }

    public void educaclik(View view) {
        Intent job=new Intent(home_page.this,education_Home.class);
        startActivity(job);
    }

    public void newsclik(View view) {
        Intent job=new Intent(home_page.this,news_Home.class);
        startActivity(job);
    }

    public void home(MenuItem item) {

    }

    public void govclick(View view) {
        Intent job=new Intent(home_page.this,gov_list.class);
        startActivity(job);
    }

    public void call_bt(View view) {
        Intent job=new Intent(home_page.this,call_list.class);
        startActivity(job);
    }
}
