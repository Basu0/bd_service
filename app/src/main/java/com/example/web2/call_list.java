package com.example.web2;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class call_list extends AppCompatActivity {
    public ListView listView;
    public String[] call_name;
    static  int d=0;
    public int[] govimage={R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,
            R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone,R.drawable.phone
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_call_list);
        listView=(ListView)findViewById( R.id.cll_list);
        call_name=getResources().getStringArray(R.array.Call_itam);
        call_adptor cu=new call_adptor(this, govimage,call_name);
        listView.setAdapter(cu);
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView <?> parent, View view, final int position, long id) {
                AlertDialog.Builder ad=new AlertDialog.Builder(call_list.this);
                ad.setTitle("Attention");
                //ad.setMessage("You Suru Call ");
                ad.setIcon(R.drawable.ic_call_black);
                ad.setPositiveButton("CALL NOW", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(position==0){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:999"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==1){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16263"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==2){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:109"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);
                        }
                        if(position==3){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:1098"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==4){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16430"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==5){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:105"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==6){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:106"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==7){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16236"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==8){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16256"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==9){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16123"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==10){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:10941"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==11){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:16402"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==12){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:100"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                        if(position==13){
                            Intent callIntent = new Intent(Intent.ACTION_CALL);
                            callIntent.setData(Uri.parse("tel:09654333333"));
                            if (ActivityCompat.checkSelfPermission(call_list.this,
                                    Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                return;}
                            startActivity(callIntent);

                        }
                    }
                });
                ad.setNegativeButton("NO CALL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                AlertDialog alert11 = ad.create();
                alert11.show();


            }
        } );
    }
}