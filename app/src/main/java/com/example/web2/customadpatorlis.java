package com.example.web2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class customadpatorlis extends BaseAdapter {
  int[] jobimage;
  String[] jobname;
  Context context;
    public LayoutInflater inflater;

    customadpatorlis(Context context,int[] jobimage,String[] jobname){
        this.context=context;
        this.jobname=jobname;
        this.jobimage=jobimage;

    }
    @Override
    public int getCount() {
        return jobname.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView= inflater.inflate(R.layout.joblist_sample_layout,parent,false);

        }
        ImageView Imageview =(ImageView)convertView.findViewById(R.id.im_chakri);
        TextView textView=(TextView)convertView.findViewById(R.id.tx_chakri);
        Imageview.setImageResource(jobimage[position]);
        textView.setText(jobname[position]);
        return convertView;

    }
}
