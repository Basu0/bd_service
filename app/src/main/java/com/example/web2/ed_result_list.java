package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class ed_result_list extends AppCompatActivity {
public ListView listView;
public String[] edresult;
public int[] edimg={R.drawable.bd_logo,R.drawable.logo_bteb,R.drawable.mbbs_banner,R.drawable.bds,R.drawable.bute,R.drawable.bcs,R.drawable.psc};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView( R.layout.ed_result_list );
        listView=(ListView)findViewById(R.id.result_List_id);
        edresult=getResources().getStringArray(R.array.edresultarry);
        result_Custom_adptor rw=new result_Custom_adptor(this,edimg,edresult);
        listView.setAdapter(rw);
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if (position==0){
                    Intent intf=new Intent(ed_result_list.this,psc_ssc_web.class);
                    startActivity( intf );
                }
                else if (position==1){
                    Intent intf=new Intent(ed_result_list.this,bteb_web.class);
                    startActivity( intf );

                }
                else if (position==2){
                    Intent intf=new Intent(ed_result_list.this,mbbs_web.class);
                    startActivity( intf );

                }
                else if (position==3){
                    Intent intf=new Intent(ed_result_list.this,bds_web.class);
                    startActivity( intf );

                }
                else if (position==4){
                    Intent intf=new Intent(ed_result_list.this,bute_web.class);
                    startActivity( intf );

                }
                else if (position==5){
                    Intent intf=new Intent(ed_result_list.this,bcs_web.class);
                    startActivity( intf );

                }
                else if (position==6){
                    Intent inty=new Intent(ed_result_list.this,bd_career_web.class);
                    inty.putExtra("url","http://dperesult.teletalk.com.bd/dpe.php");
                    startActivity( inty);

                }
            }
        } );

    }
}
