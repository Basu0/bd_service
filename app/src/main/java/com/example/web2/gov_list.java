package com.example.web2;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import static android.content.Intent.getIntent;

public class gov_list extends AppCompatActivity {
    public ListView listView;
    public String[] gov_name;
    public int[] govimage={R.drawable.gov,R.drawable.gov,R.drawable.gov,R.drawable.bank,R.drawable.gov,R.drawable.gov,
            R.drawable.rap,R.drawable.army,R.drawable.bgb,R.drawable.polic,R.drawable.nvey,R.drawable.ndc,R.drawable.rail,R.drawable.biman
            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gov_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        listView=(ListView)findViewById( R.id.govlist);
        gov_name=getResources().getStringArray(R.array.gov_item);
        addmissionAddptor cu=new addmissionAddptor(this, govimage, gov_name);
        listView.setAdapter(cu);
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(position==0){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.bangladesh.gov.bd/index.php?lang=en");
                    startActivity( inty);

                }
                if(position==1){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://pmo.gov.bd/");
                    startActivity( inty);

                }
                if(position==2){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.parliament.gov.bd/");
                    startActivity( inty);

                }
                if(position==3){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bb.org.bd/");
                    startActivity( inty);

                }
                if(position==4){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://mof.gov.bd/");
                    startActivity( inty);

                }
                if(position==5){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.educationboard.gov.bd/");
                    startActivity( inty);

                }
                if(position==6){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.rab.gov.bd/");
                    startActivity( inty);

                }
                if(position==7){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.army.mil.bd/");
                    startActivity( inty);

                }
                if(position==8){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.bgb.gov.bd/");
                    startActivity( inty);

                }
                if(position==9){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.police.gov.bd/");
                    startActivity( inty);

                }
                if(position==10){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.navy.mil.bd/index.php");
                    startActivity( inty);

                }
                if(position==11){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.ndc.gov.bd/");
                    startActivity( inty);

                }
                if(position==12){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.esheba.cnsbd.com/");
                    startActivity( inty);

                }
                if(position==13){
                    Intent inty=new Intent(gov_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.biman-airlines.com/");
                    startActivity( inty);

                }
            }
        } );
    }
}