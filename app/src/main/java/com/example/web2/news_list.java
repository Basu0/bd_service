package com.example.web2;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

public class news_list extends AppCompatActivity {
    public ListView listView;
    public String[] newname;
    public int[] newsimage={R.drawable.potomalo,R.drawable.iktapat,R.drawable.bangla,R.drawable.samakal,R.drawable.amadershomoy,R.drawable.naya_diganta,
            R.drawable.kaler_kantho,R.drawable.jai_jai_din,R.drawable.bangladesh_protidin,R.drawable.daily_inqilab,R.drawable.daily_manab_zamin,R.drawable.daily_sangram,R.drawable.daily_bhorer_agoj,R.drawable.janakantha
            ,R.drawable.jugantor,R.drawable.sangbad,R.drawable.amadarorthoneeti,R.drawable.daily_bhorer_agoj,R.drawable.dinkal,R.drawable.manob_kantha};
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView( R.layout.activity_news_list );
        listView=(ListView)findViewById( R.id.news_List_id);
        newname=getResources().getStringArray(R.array.news_name);
        newsCustomAdptor cu=new newsCustomAdptor(this, newsimage, newname);
        listView.setAdapter(cu);
        listView.setOnItemClickListener( new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView <?> parent, View view, int position, long id) {
                if(position==0){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.prothomalo.com/");
                    startActivity( inty);

                }

                if(position==1){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.ittefaq.com.bd/");
                    startActivity( inty);

                }
                if(position==2){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bn.bangla.report/");
                    startActivity( inty);

                }
                if(position==3){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://samakal.com/");
                    startActivity( inty);

                }
                if(position==4){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dainikamadershomoy.com/");
                    startActivity( inty);

                }
                if(position==5){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dailynayadiganta.com/");
                    startActivity( inty);

                }
                if(position==6){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.kalerkantho.com/");
                    startActivity( inty);

                }
                if(position==7){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.jaijaidinbd.com/");
                    startActivity( inty);

                }
                if(position==8){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.bd-pratidin.com/");
                    startActivity( inty);

                }
                if(position==9){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.dailyinqilab.com/");
                    startActivity( inty);

                }
                if(position==10){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.mzamin.com/");
                    startActivity( inty);

                }
                if(position==11){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dailysangram.com/");
                    startActivity( inty);

                }
                if(position==12){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bhorerkagoj.com/");
                    startActivity( inty);

                }
                if(position==13){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dailyjanakantha.com/");
                    startActivity( inty);

                }
                if(position==14){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.jugantor.com/");
                    startActivity( inty);

                }
                if(position==15){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.24livenewspaper.com/");
                    startActivity( inty);

                }
                if(position==16){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://amaderorthoneeti.com/new/");
                    startActivity( inty);

                }
                if(position==17){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","https://www.bhorerkagoj.com/");
                    startActivity( inty);

                }
                if(position==18){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.dailydinkal.net/2019/02/18/index.php");
                    startActivity( inty);

                }
                if(position==19){
                    Intent inty=new Intent(news_list.this,bd_career_web.class);
                    inty.putExtra("url","http://www.manobkantha.com/");
                    startActivity( inty);

                }
            }
        } );
    }
}
