package com.example.web2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class result_Custom_adptor extends BaseAdapter {
    int[] edimg;
    String[] edresult;
    Context context;
    public LayoutInflater inflater;

    result_Custom_adptor(Context context,int[] edimg,String[] edresult){
        this.context=context;
        this.edimg=edimg;
        this.edresult=edresult;

    }

    @Override
    public int getCount() {
        return edresult.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView==null){
            inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=inflater.inflate( R.layout.ed_layout,parent,false);
           }
        ImageView Imageview =(ImageView)convertView.findViewById(R.id.im_ed);
        TextView textView=(TextView)convertView.findViewById(R.id.tx_ed);
        Imageview.setImageResource(edimg[position]);
        textView.setText(edresult[position]);

        return convertView;
    }
}
