package com.example.web2;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class bd_career_web extends AppCompatActivity {
    WebView webView;
    String urlh;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.chakri_web );
        urlh=getIntent().getStringExtra("url");
        webView=(WebView) findViewById(R.id.chakriweb);
        webView.setWebViewClient(new WebViewClient() );
        WebSettings ws=webView.getSettings();
        ws.setJavaScriptEnabled(true);

        webView.loadUrl(urlh);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();

        }
        else {
            super.onBackPressed();}
    }
}